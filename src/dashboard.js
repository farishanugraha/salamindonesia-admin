var spots = [];
var url = {
	district: "http://kamtis.id/data/district.json",
	city: "http://kamtis.id/data/city.json",
	province: "http://kamtis.id/data/province.json",
	spot: "http://api.salamindonesia.id/v1/spot",
	// spot: "http://localhost:8000/v1/spot/"
	delete_spot: "http://api.salamindonesia.id/v1/delete"
}
Vue.component('multiselect', window.VueMultiselect.default)

var app = new Vue({
	el: '#app',
	data: {
		areaSearchMethod: false,
		province: '',
		city: '',
		district: '',
		province_options: [],
		city_options: [],
		district_options: [],
		result: [],
		result_length: 0
	},
	mounted(){
		$('.multiselect__spinner').show();
		getProvince();
		getAllData();
	},
	methods: {
		logout: function(e){
			console.log('logout');
			localStorage.removeItem('isAuth');
			window.location.reload();
		},
		getSpot: function(e){
			this.renderSpot('all', false);
		},
		getArea: function(e){
			this.areaSearchMethod = !this.areaSearchMethod
		},
		getByProvince: function(val){
			this.renderSpot('province', val);
		},
		getByCity: function(val){
			this.renderSpot('city', val);
		},
		getByDistrict: function(val){
			this.renderSpot('district', val);
		},
		renderSpot: function(type, val){
			if(val){
				$('#spots').html('');
				this.result = [];
				spots.map((data, i)=>{
					if(type=="province"){
						if(val.name == data.guide.address.province){
							var spot = renderHtml(data);
							this.result.push(spot);
							$('#spots').append(spot);
						}
					}
					else if (type=="city"){
						if(val.name == data.guide.address.city){
							var spot = renderHtml(data);
							this.result.push(spot);
							$('#spots').append(spot);
						}
					}
					else if (type=="district"){
						if(val.name == data.guide.address.district){
							var spot = renderHtml(data);
							this.result.push(spot);
							$('#spots').append(spot);
						}
					}else{
						alert('error');
					}
				});
			}else{
				$('#spots').html('');
				this.result = [];
				spots.map((data, i)=>{
					var spot = renderHtml(data);
					this.result.push(spot);
					$('#spots').append(spot);
				});
			}
		},
		generatePDF: function(e){
			var doc = new jsPDF({
				orientation: 'l',
				unit: 'px',
				format: 'a4'
			});
			var specialElementHandlers = {
			    '#editor': function (element, renderer) {
			        return true;
			    }
			};
		    doc.fromHTML($('#spots').html(), 15, 15, {
		    	'width': 170,
		        'fontName': "Open Sans",
		        'elementHandlers': specialElementHandlers
		    }, function(res){
		    	if(res.ready){
				    doc.save('salam-indonesia.pdf');
				    alert('sukses');
		    	}else{
		    		alert('gagal');
		    	}
		    });
		},
		deleteAll: function(e){
			var c = confirm("Yakin ingin menghapus semua data?");
			if(c){
				$.ajax({
					method: "DELETE",
					url: url.spot,
					success: function(res){
						alert('All assets deleted.');
						window.location.reload();
					},
					error: function(e){
						alert('Error.');
						window.location.reload();
					}
				});
			}
		}
	},
	watch: {
		province: function(val){
			getCity(val.id);
			this.getByProvince(val);
		},
		city: function(val){
			getDistrict(val.id);
			this.getByCity(val);
		},
		district: function(val){
			this.getByDistrict(val);
		},
		result: function(){
			this.result_length = this.result.length;
			if(this.result_length==0){
				$('#result_length').html('<i class="text-muted">No result.</i>');
			}else{
				$('#result_length').html('<i class="text-muted">Result: '+this.result_length+'</i>');
			}
		}
	}
});

function getProvince(){
	$.getJSON(url.province, function(json) {
		json.map((data, i)=>{
			app.province_options.push(data);
		});
		$('.multiselect__spinner').hide('fast');
	});
}

function getCity(id){
	app.city_options = [];
	$('.multiselect__spinner').show('fast');
	$.getJSON(url.city, function(json) {
		json.map((data, i)=>{
			if(data.province_id == id){
				app.city_options.push(data);
			}
		});
		$('.multiselect__spinner').hide('fast');
	});
}

function getDistrict(id){
	app.district_options = [];
	$('.multiselect__spinner').show('fast');
	$.getJSON(url.district, function(json) {
		json.map((data, i)=>{
			if(data.regency_id == id){
				app.district_options.push(data);
			}
		});
		$('.multiselect__spinner').hide('fast');
	});
}

function getAllData(){
	$.ajax({
		method: "GET",
		crossDomain: true,
		dataType: 'json',
		url: url.spot,
		success: function(res){
			res.map((data, i)=>{
				spots.push(data);
			});
		},
		error: function(err){
			console.log(err)
		}
	});
}

function renderHtml(data, imgs){
	// render social media
	var x = data.guide.social_media.instagram;
	var x1 = data.guide.social_media.facebook;
	var x2 = data.guide.social_media.twitter;
	var xx = x.indexOf('@');
	var xx1 = x1.indexOf('@');
	var xx2 = x2.indexOf('@');
	var ig = '';
	var fb = '';
	var tw = '';
	if(xx==0){
		ig = 'https://instagram.com/'+x.substr(1);
	}else{
		ig = 'https://instagram.com/'+x;
	}
	if(xx1==0){
		fb = 'https://facebook.com/'+x1.substr(1);
	}else{
		fb = 'https://facebook.com/'+x1;
	}
	if(xx2==0){
		tw = 'https://twitter.com/'+x2.substr(1);
	}else{
		tw = 'https://twitter.com/'+x2;
	}
	if(!data.guide.email){
		data.guide.email = '<i>no email</i>';
	}
	var imgs = '';
	for (var i = 0; i < data.spot.media.length; i++) {
		var img = '<img src="'+data.spot.media[i]+'">';
		imgs += img;
	}
	if(imgs == ''){
		imgs = '<i class="text-muted">No images.</i>';
	}
	return '<div class="spot col-md-3"><h4><button class="btn btn-danger" onclick="deleteThis(\''+data._id+'\')">&times;</button><b>'+data.spot.name+'</b>, '+data.spot.address+'</h4><h5> '+data.guide.address.city+', '+data.guide.address.province+'</h5><div class="media">'+imgs+'</div><br><p>'+data.spot.description+' <br><b>Additional:</b> '+data.spot.additional+'</p><hr>'+'<p><b>'+data.guide.name+'</b> | '+data.guide.phone+' | '+data.guide.email+'</p><p>facebook: <a href="'+fb+'" target="_blank">'+data.guide.social_media.facebook+'</a> | twitter: <a href="'+tw+'" target="_blank">'+data.guide.social_media.twitter+'</a> | instagram: <a href="'+ig+'" target="_blank">'+data.guide.social_media.instagram+'</a></p><p>'+data.guide.address.street+', '+data.guide.address.district+', '+data.guide.address.city+', '+data.guide.address.province+'</p>'+'</div>';
}

function deleteThis(id){
	console.log('Delete this:', id)
	var c = confirm('Yakin ingin menghapus data ini?');
	if(c){
		$.ajax({
			method: 'GET',
			url: url.delete_spot+'/'+id,
			success: function(res){
				console.log('SUCCESS', res);
				alert('Data berhasil dihapus.');
				window.location.reload();
			},
			error: function(err){
				alert('error.');
				console.log('ERROR', err)	
			}
		});
	}else{
		console.log('user canceled');
	}
}
